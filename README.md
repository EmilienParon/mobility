Mobility
========================================
Mobility is a package developed by Elioth (https://elioth.com/) to :
- Get a sample of all trips that a person makes in a given time frame (one to several years), with information about their purposes, their transport modes, their distance and their GHG emissions.
- Model the impact of a modification of the balance jobs/residents or shops/clients in an area, at the city level.

The model covers French cities only. It is based on several public datasets : ENTD (SDES), census detailed data (INSEE), base carbone (ADEME). It implements the "Universal opportunity model for human mobility", developped by Liu and Yan in (https://www.nature.com/articles/s41598-020-61613-y).

Large files are not included in this repository to keep it light. If you want to use the package please contact [Félix Pouchain](https://gitlab.com/FlxPo) to get a zipped copy of the necessary files.

License
---------------------
Copyright elioth

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.

You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

Contributors
---------------------
- [Félix Pouchain](https://gitlab.com/FlxPo)
- [Louise Gontier](https://gitlab.com/l.gontier)
